import * as types from '../constants/actionTypes';
import {ExchangeRates} from "../interfaces/ExchangeRates";

export function changeFromCurrency(value: string) {
  return {
    type: types.CHANGE_FROM_CURRENCY,
    value,
  };
}

export function changeToCurrency(value: string) {
  return {
    type: types.CHANGE_TO_CURRENCY,
    value,
  };
}

export function changeFromAmount(value: number) {
  return {
    type: types.CHANGE_FROM_AMOUNT,
    value,
  };
}

export function updateExchangeRates(value: ExchangeRates) {
  return {
    type: types.UPDATE_EXCHANGE_RATES,
    value,
  };
}

export function exchange() {
  return {
    type: types.EXCHANGE,
  };
}
