import * as ActionTypes from '../constants/actionTypes';
import {changeFromAmount, changeFromCurrency, changeToCurrency, updateExchangeRates, exchange} from './exchangeActions';

describe('exchangeActions', () => {
  it('should create an action to change from currency', () => {
    const currency = 'USD';
    const actual = changeFromCurrency(currency);
    const expected = {
      type: ActionTypes.CHANGE_FROM_CURRENCY,
      value: currency,
    };

    expect(actual).toEqual(expected);
  });

  it('should create an action to change to currency', () => {
    const currency = 'EUR';
    const actual = changeToCurrency(currency);
    const expected = {
      type: ActionTypes.CHANGE_TO_CURRENCY,
      value: currency,
    };

    expect(actual).toEqual(expected);
  });

  it('should create an action to change from amount', () => {
    const amount = 100;
    const actual = changeFromAmount(amount);
    const expected = {
      type: ActionTypes.CHANGE_FROM_AMOUNT,
      value: amount,
    };

    expect(actual).toEqual(expected);
  });

  it('should create an action to update exchange rates', () => {
    const exchangeRates = {
      EUR: 1.23,
      GBP: 1.33,
    };
    const actual = updateExchangeRates(exchangeRates);
    const expected = {
      type: ActionTypes.UPDATE_EXCHANGE_RATES,
      value: exchangeRates,
    };

    expect(actual).toEqual(expected);
  });

  it('should create an action to exchange', () => {
    const actual = exchange();
    const expected = {
      type: ActionTypes.EXCHANGE,
    };

    expect(actual).toEqual(expected);
  });
});
