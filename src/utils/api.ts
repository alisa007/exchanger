import axios from "axios";
import {Currency} from "../interfaces";
import {ExchangeRates} from "../interfaces/ExchangeRates";

interface GetRatesResponse {
  data: {
    rates: ExchangeRates;
  }
}

export function calcRate(fromCurrency: string, toCurrency: string,
                         baseCurrency: string, exchangeRates: ExchangeRates): number {
  const toRate = exchangeRates[toCurrency] || 1;
  const unformattedRate = fromCurrency === baseCurrency
    ? toRate
    : 1 / (exchangeRates[fromCurrency] || 1) * toRate;

  return Math.round(unformattedRate * 100)/100;
}

export function getRates(baseCurrency: string, currencies: Currency[]) {
  const appId = "6cc19693649246b59f7f9cade74c9deb";

  return axios('https://openexchangerates.org/api/latest.json', {
    params: {
      base: baseCurrency,
      app_id: appId,
      symbols: currencies
        .map(c => c.name)
        .filter(c => c !== baseCurrency)
        .join(","),
    }
  }).then((response: GetRatesResponse) => response.data.rates);
}
