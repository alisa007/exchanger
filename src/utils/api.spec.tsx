import {calcRate} from './api';

describe('Api Helper', () => {
  describe('calcRate', () => {
    const calcDefaultRate = (fromCurrency: string, toCurrency: string) => {
      return calcRate(
        fromCurrency,
        toCurrency,
        'USD',
        {
          EUR: 1.11,
          GBP: 1.22
        });
    };

    it('returns 0 when exchange rate is correct', () => {
      expect(calcDefaultRate('USD', 'USD')).toEqual(1);
    });

    it('returns 0 when exchange rate is correct', () => {
      expect(calcDefaultRate('USD', 'EUR')).toEqual(1.11);
    });

    it('returns 0 when exchange rate is correct', () => {
      expect(calcDefaultRate('USD', 'GBP')).toEqual(1.22);
    });

    it('returns 0 when exchange rate is correct', () => {
      expect(calcDefaultRate('EUR', 'USD')).toEqual(0.9);
    });

    it('returns 0 when exchange rate is correct', () => {
      expect(calcDefaultRate('EUR', 'EUR')).toEqual(1);
    });

    it('returns 0 when exchange rate is correct', () => {
      expect(calcDefaultRate('EUR', 'GBP')).toEqual(1.1);
    });

    it('returns 0 when exchange rate is correct', () => {
      expect(calcDefaultRate('GBP', 'USD')).toEqual(0.82);
    });

    it('returns 0 when exchange rate is correct', () => {
      expect(calcDefaultRate('GBP', 'EUR')).toEqual(0.91);
    });

    it('returns 0 when exchange rate is correct', () => {
      expect(calcDefaultRate('GBP', 'GBP')).toEqual(1);
    });
  });
});
