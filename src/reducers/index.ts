import { combineReducers } from 'redux';
import exchange from './exchangeReducer';

const rootReducer = combineReducers({
  exchange,
});

export default rootReducer;
