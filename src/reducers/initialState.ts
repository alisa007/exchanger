import {AppState} from "../interfaces/AppState";

const baseCurrency = "USD";
const currencies = [{
  name: "USD",
  sign: "$",
  amount: 100,
}, {
  name: "EUR",
  sign: "€",
  amount: 100,
}, {
  name: "GBP",
  sign: "£",
  amount: 100,
}];

const initialState: AppState = {
  exchange: {
    currencies: currencies,
    from: {
      currency: currencies[0].name,
      amount: currencies[0].amount,
    },
    to: {
      currency: currencies[1].name,
    },
    baseCurrency,
  }
};

export default initialState;
