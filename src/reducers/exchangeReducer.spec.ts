import * as ActionTypes from '../constants/actionTypes';
import reducer from './exchangeReducer';
import initialState from './initialState';

describe('Reducers::exchangeReducer', () => {
  const getInitialState = () => {
    return initialState.exchange;
  };

  const getAppState = () => {
    return {
      ...getInitialState(),
      selectedPairExchangeRate: 1.11,
      exchangeRates: {
        EUR: 1.11,
        GBP: 1.22,
      }
    };
  };

  it('should set initial state by default', () => {
    const action = { type: 'unknown' };
    const expected = getInitialState();

    expect(reducer(undefined, action)).toEqual(expected);
  });

  it('should handle CHANGE_FROM_CURRENCY', () => {
    const action = { type: ActionTypes.CHANGE_FROM_CURRENCY, value: 'USD' };
    const expected = Object.assign(getAppState());

    expect(reducer(getAppState(), action)).toEqual(expected);
  });

  it('should handle CHANGE_TO_CURRENCY', () => {
    const action = { type: ActionTypes.CHANGE_TO_CURRENCY, value: 'GBP' };
    const expected = Object.assign(
      getAppState(),
      { to: { currency: 'GBP' }},
      { selectedPairExchangeRate: 1.22 }
    );

    expect(reducer(getAppState(), action)).toEqual(expected);
  });

  it('should handle CHANGE_FROM_AMOUNT', () => {
    const action = { type: ActionTypes.CHANGE_FROM_AMOUNT, value: 100 };
    const expected = Object.assign(getAppState());

    expect(reducer(getAppState(), action)).toEqual(expected);
  });

  it('should handle UPDATE_EXCHANGE_RATES', () => {
    const action = { type: ActionTypes.UPDATE_EXCHANGE_RATES, value: {
      EUR: 1.11,
      GBP: 1.22,
    }};
    const expected = Object.assign(getAppState());

    expect(reducer(getAppState(), action)).toEqual(expected);
  });

  it('should handle EXCHANGE', () => {
    const action = { type: ActionTypes.EXCHANGE };

    expect(reducer(getAppState(), action).currencies[0].amount).toEqual(0);
    expect(reducer(getAppState(), action).currencies[1].amount).toEqual(211);
  });
});
