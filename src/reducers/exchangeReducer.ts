import {
  CHANGE_FROM_CURRENCY,
  CHANGE_TO_CURRENCY,
  UPDATE_EXCHANGE_RATES,
  CHANGE_FROM_AMOUNT,
  EXCHANGE,
} from '../constants/actionTypes';
import initialState from './initialState';
import {ExchangeAction, ExchangeState} from "../interfaces";
import {calcRate} from "../utils/api";

export default function exchangeReducer(state = initialState.exchange, action: ExchangeAction): ExchangeState {
  switch (action.type) {
    case CHANGE_FROM_CURRENCY:
      return {
        ...state,
        from: { ...state.from, currency: action.value },
        selectedPairExchangeRate: calcRate(action.value, state.to.currency, state.baseCurrency, state.exchangeRates),
      };

    case CHANGE_TO_CURRENCY:
      return {
        ...state,
        to: { ...state.to, currency: action.value },
        selectedPairExchangeRate: calcRate(state.from.currency, action.value, state.baseCurrency, state.exchangeRates),
      };

    case CHANGE_FROM_AMOUNT:
      return {...state, from: { ...state.from, amount: action.value }};

    case UPDATE_EXCHANGE_RATES:
      return {
        ...state,
        exchangeRates: action.value,
        selectedPairExchangeRate: calcRate(state.from.currency, state.to.currency, state.baseCurrency, action.value),
      };

    case EXCHANGE:
      return state.from.currency === state.to.currency ? state : {
        ...state,
        from: { ...state.from, amount: 0 },
        currencies: state.currencies.map(c => {
          if (c.name === state.from.currency) {
            return {
              ...c,
              amount: +(c.amount - state.from.amount).toFixed(2),
            };
          }

          if (c.name === state.to.currency) {
            const amount = state.from.amount * state.selectedPairExchangeRate;

            return {
              ...c,
              amount: +(c.amount + amount).toFixed(2),
            };
          }

          return c;
        })};

    default:
      return state;
  }
}
