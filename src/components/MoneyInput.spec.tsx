import React from 'react';
import { mount } from 'enzyme';
import MoneyInput from './MoneyInput';
import initialState from "../reducers/initialState";

describe('<MoneyInputFormat />', () => {
  const defaultProps = {
    title: 'title',
    value: 100,
    onChange: jest.fn(),
    onCurrencyChange: jest.fn(),
    currencies: initialState.exchange.currencies,
    selectedCurrency: 'USD',
  };

  it('should format the value', () => {
    const props = {
      ...defaultProps,
      value: 10000.666
    };

    const wrapper = mount(<MoneyInput {...props} />);
    const input = wrapper.find('input[type="text"]');

    expect(input.props().value).toEqual("10,000.67");
  });
});
