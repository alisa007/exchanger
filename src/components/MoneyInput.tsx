import React from 'react';
import CurrencySelector from './CurrencySelector';
import TextField from '@material-ui/core/TextField';
import { Currency} from '../interfaces';
import NumberFormat from 'react-number-format';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  root: {
    width: '100%',
  },
};

export interface NumberFormatCustomProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (value: number) => void;
  defaultValue: number;
}

function MoneyInputFormat(props: NumberFormatCustomProps) {
  const { inputRef, defaultValue, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => onChange(values.floatValue)}
      isAllowed={values => {
        return values.floatValue <= defaultValue;
      }}
      thousandSeparator
      decimalScale={2}
    />
  );
}

interface MoneyInputProps {
  classes: any;
  title: string;
  value: number;
  disabled?: boolean;
  onChange?: (value: any) => void;
  onCurrencyChange: (value: string) => void;
  currencies: Currency[];
  selectedCurrency: string;
}

const MoneyInput = (props: MoneyInputProps) => {
  const { classes, title, disabled, value, onChange, currencies, selectedCurrency, onCurrencyChange } = props;

  return (
    <TextField
      className={classes.root}
      disabled={disabled}
      variant="outlined"
      label={title}
      value={value}
      defaultValue={currencies.find(c => c.name === selectedCurrency).amount}
      onChange={onChange}
      InputProps={{
        startAdornment: CurrencySelector({ currencies, selectedCurrency, change: onCurrencyChange }),
        inputComponent: MoneyInputFormat as any,
      }}
    />
  );
};

export default withStyles(styles)(MoneyInput);
