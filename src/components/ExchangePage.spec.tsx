import React from 'React';
import { mount } from 'enzyme';
import ExchangePage from './ExchangePage';
import initialState from "../reducers/initialState";

describe('<ExchangePage />', () => {
  const defaultProps = {
    currencies: initialState.exchange.currencies,
    fromCurrency: 'USD',
    toCurrency: 'EUR',
    fromAmount: 100,
    exchangeRate: 1.11,
    onFromCurrencyChange: jest.fn(),
    onToCurrencyChange: jest.fn(),
    onAmountChange: jest.fn(),
    onExchange: jest.fn(),
  };

   it('should show exchange page', () => {
     const wrapper = mount(<ExchangePage {...defaultProps} />);

     expect(wrapper.text()).toEqual('from​$to​€ExchangeExchange rate: USD1 = EUR1.11Balance: $100 + €100 + £100');
  });
});
