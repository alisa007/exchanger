import React from 'React';
import { mount } from 'enzyme';
import CurrencySelector from './CurrencySelector';
import initialState from "../reducers/initialState";

describe('<ExchangePage />', () => {
  const defaultProps = {
    currencies: initialState.exchange.currencies,
    selectedCurrency: 'USD',
    change: jest.fn(),
  };

  it('should show currency selector', () => {
    const wrapper = mount(<CurrencySelector {...defaultProps} />);

    expect(wrapper.text()).toEqual('$');
  });
});
