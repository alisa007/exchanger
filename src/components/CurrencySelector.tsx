import React from "react";
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import MenuItem from '@material-ui/core/MenuItem';
import { Currency } from '../interfaces';

interface CurrencySelectorProps {
  currencies: Currency[];
  selectedCurrency: string;
  change: (value: string) => void;
}

const CurrencySelector = (props: CurrencySelectorProps) => {
  const { currencies, selectedCurrency, change } = props;

  return (
    <InputAdornment position="start">
      <TextField
        select
        value={selectedCurrency}
        onChange={values => change(values.target.value)}>
        {currencies
          .map(option => (
            <MenuItem key={option.name}
                      value={option.name}>
              {option.sign}
            </MenuItem>
          ))}
      </TextField>
    </InputAdornment>
  );
};

export default CurrencySelector;
