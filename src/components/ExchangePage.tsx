import Grid from '@material-ui/core/Grid';
import MoneyInput from './MoneyInput';
import React from "react";
import Typography from '@material-ui/core/Typography';
import {Currency} from "../interfaces";
import Button from "@material-ui/core/Button";

interface ExchangePageProps {
  currencies: Currency[];
  fromCurrency: string;
  toCurrency: string;
  fromAmount: number;
  exchangeRate: number;
  onToCurrencyChange: (value: string) => void;
  onFromCurrencyChange: (value: string) => void;
  onAmountChange: (value: number) => void;
  onExchange: () => void
}

const ExchangePage = (props: ExchangePageProps) => {
  const { currencies, fromCurrency, toCurrency, fromAmount, exchangeRate,
    onFromCurrencyChange, onToCurrencyChange, onAmountChange, onExchange } = props;

  return (
    <Grid container spacing={8}>
      <Grid item xs={12} sm={6}>
        <MoneyInput title="from"
                    currencies={currencies}
                    selectedCurrency={fromCurrency}
                    onCurrencyChange={onFromCurrencyChange}
                    value={fromAmount}
                    onChange={onAmountChange}/>
      </Grid>
      <Grid item xs={12} sm={6}>
        <MoneyInput title="to"
                    currencies={currencies}
                    selectedCurrency={toCurrency}
                    onCurrencyChange={onToCurrencyChange}
                    value={fromAmount * exchangeRate}
                    disabled={true}/>
      </Grid>
      <Button variant="contained"
              fullWidth={true}
              color="primary"
              onClick={onExchange}
      >
        Exchange
      </Button>

      <div>
        <Typography variant="body1" gutterBottom>
          Exchange rate: {fromCurrency}1 = {toCurrency}{exchangeRate}
        </Typography>
        <Typography variant="body1" gutterBottom>
          Balance: {currencies.map(c => c.sign + c.amount).join(' + ')}
        </Typography>
      </div>
    </Grid>
  );
};

export default ExchangePage;
