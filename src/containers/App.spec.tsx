import React from "react";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import { create } from "react-test-renderer";
import App from "./App";
import initialState from "../reducers/initialState";

describe("<FuelSavingsPage />", () => {
  it("should match ready snapshot", () => {
    const store = configureMockStore()({
      ...initialState,
      exchangeRates: {
        EUR: 1.11,
        GBP: 1.22,
      }
    });
    const component = create(
      <Provider store={store}>
        <App />
      </Provider>
    );
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});
