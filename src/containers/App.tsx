import * as React from "react";
import { hot } from "react-hot-loader";
import { timer } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { connect } from "react-redux";
import {AnyAction, bindActionCreators, Dispatch} from "redux";
import * as actions from "../actions/exchangeActions";
import { getRates } from '../utils/api';
import ExchangePage from "../components/ExchangePage";
import CircularProgress from '@material-ui/core/CircularProgress';
import {AppState, ExchangeState, ExchangeActions} from "../interfaces";

interface AppProps {
  exchange: ExchangeState;
  actions: ExchangeActions;
}

class App extends React.Component {
  props: AppProps;

  constructor(props: AppProps) {
    super(props);

    const sec = 1000;
    const { baseCurrency, currencies } = props.exchange;

    timer(0, 10 * sec)
      .pipe(
        flatMap(() => getRates(baseCurrency, currencies))
      )
      .subscribe((rates) => {
        props.actions.updateExchangeRates(rates)
      });
  }

  render() {
    const { changeFromCurrency, changeToCurrency, changeFromAmount, exchange } = this.props.actions;
    const { from, to, currencies, exchangeRates, selectedPairExchangeRate } = this.props.exchange;

    return (
      exchangeRates
        ? <ExchangePage
            currencies={currencies}
            fromCurrency={from.currency}
            toCurrency={to.currency}
            fromAmount={from.amount}
            exchangeRate={selectedPairExchangeRate}
            onFromCurrencyChange={changeFromCurrency}
            onToCurrencyChange={changeToCurrency}
            onAmountChange={changeFromAmount}
            onExchange={exchange}
          />
        : <CircularProgress  />
    );
  }
}

function mapStateToProps(state: AppState) {
  return state;
}

function mapDispatchToProps(dispatch: Dispatch<AnyAction>) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

const app = connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);

export default hot(module)(app);
