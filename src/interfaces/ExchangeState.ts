import {Currency} from "./Currency";
import {ExchangeRates} from "./ExchangeRates";

export interface ExchangeState {
  currencies: Currency[],
  from: {
    currency: string;
    amount: number;
  },
  to: {
    currency: string;
  },
  baseCurrency: string;
  exchangeRates?: ExchangeRates;
  selectedPairExchangeRate?: number;
}
