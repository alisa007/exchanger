import {ExchangeState} from "./ExchangeState";

export interface AppState {
  exchange: ExchangeState;
}
