export interface ExchangeAction {
  type: string;
  value?: any;
}
