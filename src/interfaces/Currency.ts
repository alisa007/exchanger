export interface Currency {
  sign: string;
  name: string;
  amount: number;
}
