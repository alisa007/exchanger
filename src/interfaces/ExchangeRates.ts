export interface ExchangeRates {
  EUR: number;
  GBP: number;
  [value: string]: number;
}
