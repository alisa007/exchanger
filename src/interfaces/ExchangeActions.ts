import {ExchangeAction} from "./ExchangeAction";
import {ExchangeRates} from "./ExchangeRates";

export interface ExchangeActions {
  changeFromCurrency: (value: string) => ExchangeAction;
  changeToCurrency: (value: string) => ExchangeAction;
  changeFromAmount: (value: number) => ExchangeAction;
  updateExchangeRates: (value: ExchangeRates) => ExchangeAction;
  exchange: () => ExchangeAction;
}
